#include <iostream>
#include "Player.h"
#include "Game.h"
#include "GameOverState.h"
#include <SDL_ttf.h>

Player::Player(const LoaderParams* pParams) :SDLGameObject(pParams)
{
}

void Player::draw()
{
	SDLGameObject::draw(); // we now use SDLGameObject
}

void Player::update()
{
	

	if (m_dying)
	{
		m_dyingTime++;
		m_currentFrame = int(((SDL_GetTicks() / 100) % 9));
	}
	else
	{
		m_currentFrame = int(((SDL_GetTicks() / 100) % 5));
		score++;
	}

	std::cout << "Score: " << score << std::endl;

	m_acceleration.setX(-0.01); //page 77

	handleInput();

	if (m_dying)
	{
		m_velocity.setX(0);
		m_velocity.setY(0);
	}

	if (m_dyingTime > 100)
	{
		TheGame::Instance()->getStateMachine()->pushState(new GameOverState());
	}

	SDLGameObject::update();
}

void Player::clean()
{
}

void Player::collision()
{
	m_textureID = "explosion";
	m_currentFrame = 0;
	//m_numFrames = 9;
	m_width = 60;
	m_height = 60;

	
	m_dying = true;
}

void Player::handleInput()
{

	Vector2D* target = TheInputHandler::Instance()->getMousePosition(); //page 117
	m_velocity = *target - m_position;
	m_velocity /= 50;

//	std::cout << "HandleInput called " << "\n";
//	if(TheInputHandler::Instance()->getMouseButtonState(LEFT))
//	{
//		m_velocity.setX(1);
//	}

//	Vector2D* vec = TheInputHandler::Instance()->getMousePosition();
//	m_velocity = (*vec - m_position) / 100;
if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_RIGHT))
	{
		m_velocity.setX(2);
	//	std::cout << "Key down right key detected " << "\n";
	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_LEFT))
	{
		m_velocity.setX(-2);
	//	std::cout << "Key down left key detected " << "\n";
	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_UP))
	{
		m_velocity.setY(-2);
	}
	if(TheInputHandler::Instance()->isKeyDown(SDL_SCANCODE_DOWN))
	{
	m_velocity.setY(2);
	}
	
}


/*
void Player::draw(SDL_Renderer* pRenderer)
{
	GameObject::draw(pRenderer);
}

void Player::update()
{
	//using the fish sprite which has 5 frames
	m_x += 2;

m_currentFrame = int(((SDL_GetTicks() / 100) % 6));
}
*/


