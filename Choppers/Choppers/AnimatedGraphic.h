#ifndef SDL_AnimatedGraphic_h
#define SDL_AnimatedGraphic_h

#include <SDL.h>
#include <SDL_image.h>
#include "SDLGameObject.h"


class AnimatedGraphic : public SDLGameObject
{
public:
	AnimatedGraphic(const LoaderParams *pParams,int animSpeed);


	virtual void draw();
	virtual void update();
	virtual void clean();

	// new load function


private:
	int m_animSpeed;
};


#endif