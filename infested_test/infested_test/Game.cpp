
#include "Game.h"
#include "TextureManager.h"
#include "InputHandler.h"
#include "MainMenuState.h"
#include "GameObjectFactory.h"
#include "MenuButton.h"
#include "AnimatedGraphic.h"
#include "Player.h"
#include "ScrollingBackground.h"
#include "SoundManager.h"
#include "Crawler.h"
#include "Level1Boss.h"
#include "GameOverState.h"
#include <SDL_ttf.h>

//#include "Zombie.h"
//#include "PowerUp.h"
//#include "HealthPowerUp.h"
#include <iostream>

using namespace std;

Game* Game::s_pInstance = 0;


Game::Game():
window(0),
m_pRenderer(0),
m_bRunning(false),
m_pGameStateMachine(0),
m_playerLives(3),
m_scrollSpeed(0.0),
m_bLevelComplete(false),
m_bChangingState(false)
{
    // add some level files to an array
    m_levelFiles.push_back("assets/map1.tmx");
    m_levelFiles.push_back("assets/map3.tmx");
    
    // start at this level
    m_currentLevel = 1;
}

Game::~Game()
{
    // we must clean up after ourselves to prevent memory leaks
    m_pRenderer= 0;
    window = 0;
}


bool Game::init(const char* title, int xpos, int ypos, int width, int height, bool fullscreen)
{
    int flags = 0;
    
    // store the game width and height
    m_gameWidth = width;
    m_gameHeight = height;

	
    
    if(fullscreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }
    
    // attempt to initialise SDL
    if(SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
		
        cout << "SDL init success\n";
        // init the window
		SDL_Surface *background = SDL_LoadBMP("bg.bmp");
		/*if (background != NULL)
		{
			SDL_ShowSimpleMessageBox(0, "Background init error", SDL_GetError(), m_pWindow);
		}*/
        window = SDL_CreateWindow(title, xpos, ypos, width, height, flags);
        
        if(window != 0) // window init success
        {
            cout << "window creation success\n";
            m_pRenderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
            
            if(m_pRenderer != 0) // renderer init success
            {
                cout << "renderer creation success\n";
                //SDL_SetRenderDrawColor(m_pRenderer, 0,0,0,255);
				TheTextureManager::Instance()->load("assets/bg.bmp","background", getRenderer());
				//SDL_SetRenderDrawColor(m_pRenderer, 255, 0, 0, 255);
            }
            else
            {
                cout << "renderer init fail\n";
                return false; // renderer init fail
            }
        }
        else
        {
            cout << "window init fail\n";
            return false; // window init fail
        }
    }
    else
    {
        cout << "SDL init fail\n";
        return false; // SDL init fail
    }
    
    // add some sound effects - TODO move to better place
    TheSoundManager::Instance()->load("assets/bg.ogg", "music1", SOUND_MUSIC);
    TheSoundManager::Instance()->load("assets/boom.wav", "explode", SOUND_SFX);
    TheSoundManager::Instance()->load("assets/gun.wav", "shoot", SOUND_SFX);
	TheSoundManager::Instance()->load("assets/power.wav", "power", SOUND_SFX);
    
    TheSoundManager::Instance()->playMusic("music1", 37);
    
    //TheInputHandler::Instance()->initialiseJoysticks();
    
    //register the types for the game
    TheGameObjectFactory::Instance()->registerType("MenuButton", new MenuButtonCreator());
    TheGameObjectFactory::Instance()->registerType("Player", new PlayerCreator());
    TheGameObjectFactory::Instance()->registerType("AnimatedGraphic", new AnimatedGraphicCreator());
    TheGameObjectFactory::Instance()->registerType("ScrollingBackground", new ScrollingBackgroundCreator());
    //TheGameObjectFactory::Instance()->registerType("Glider", new GliderCreator());
    //TheGameObjectFactory::Instance()->registerType("ShotGlider", new ShotGliderCreator());
    TheGameObjectFactory::Instance()->registerType("Crawler", new CrawlerCreator());
    TheGameObjectFactory::Instance()->registerType("Level1Boss", new Level1BossCreator());
	//TheGameObjectFactory::Instance()->registerType("Zombie", new ZombieCreator());
	//TheGameObjectFactory::Instance()->registerType("health", new HealthPowerUpCreator());
    
    // start the menu state
    m_pGameStateMachine = new GameStateMachine();
    m_pGameStateMachine->changeState(new MainMenuState());
    
    m_bRunning = true; // everything inited successfully, start the main loop
    return true;
}

void Game::setCurrentLevel(int currentLevel)
{
    m_currentLevel = currentLevel;
    m_pGameStateMachine->changeState(new GameOverState());
    m_bLevelComplete = false;
}

void Game::render()
{
    SDL_RenderClear(m_pRenderer);
    

		m_pGameStateMachine->render();


    SDL_RenderPresent(m_pRenderer);
}

void Game::update()
{
	
		m_pGameStateMachine->update();

}

void Game::handleEvents()
{

		TheInputHandler::Instance()->update();

}

void Game::clean()
{
    cout << "cleaning game\n";
    
    TheInputHandler::Instance()->clean();
    
    m_pGameStateMachine->clean();
    
    m_pGameStateMachine = 0;
    delete m_pGameStateMachine;
    
    TheTextureManager::Instance()->clearTextureMap();
    
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(m_pRenderer);
    SDL_Quit();
}

void Game::setPlayerScore()
{
	m_playerScore += 100;
}


