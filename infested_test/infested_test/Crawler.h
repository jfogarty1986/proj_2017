

#ifndef SDL_Game_Programming_Book_Escalator_h
#define SDL_Game_Programming_Book_Escalator_h

#include "Enemy.h"
#include "Level.h"
#include "BulletHandler.h"

class Crawler : public Enemy 
{
public:
    
    virtual ~Crawler() {}
	
    Crawler() : Enemy()
    {
        m_dyingTime = 50;
        m_health = 10;
        m_moveSpeed = 3;
        m_bulletFiringSpeed = 0;
		
    }
    
    virtual void collision()
    {
        m_health -= 1;
        if(m_health == 0)
        {
            if(!m_bPlayedDeathSound)
            {
                TheSoundManager::Instance()->playSound("explode", 0);
                
                m_textureID = "largeexplosion";
                m_currentFrame = 0;
                m_numFrames = 9;
                m_width = 60;
                m_height = 60;
                m_bDying = true;
            }
            
        }
    }

    
    virtual void update()
    {

        if(!m_bDying)
        {   
			
			if (SDL_GetTicks() % 48 == 1)
			{
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(-3, 0));
				TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(0, -3));
				m_velocity.setX(m_moveSpeed);
				if (m_position.getX() < TheGame::Instance()->getGameWidth() / 2)
				{
					m_angle = 0;
				}
				else
				{
					m_angle = 180;
				}
			}
			
			if (move == 0)
			{
				m_velocity.setY(m_moveSpeed);
			}
			else if (move == 1)
			{
				m_velocity.setY(-m_moveSpeed);
				if (m_position.getY() <= 620 && m_position.getY() >= 610)
				{
					//TheBulletHandler::Instance()->addEnemyBullet(m_position.getX(), m_position.getY(), 16, 16, "bullet1", 1, Vector2D(-3, 0));
				}
			}
			else if (move == 3)
			{
				m_moveSpeed = 3;
				m_velocity.setX(m_moveSpeed);
			}
			else if (move == 4)
			{
				m_velocity.setX(-m_moveSpeed);
			}

			if (m_position.getY() <= 50)
			{
				move = 0;
			}
				
			else if (m_position.getY() >= TheGame::Instance()->getGameHeight() - 50)
			{
				move = 1;
				
			}
			else if (m_position.getX() >= TheGame::Instance()->getGameWidth() - 50)
			{
				move = 4;
			}
			else if (m_position.getX() <= 50)
			{
				move = 3;
			}
			
  
			
            
        }
        else
        {
            m_velocity.setY(0);
            doDyingAnimation();
			TheGame::Instance()->setPlayerScore();
        }

		
        
        ShooterObject::update();
    }

	

private:
	int move = 0;
	int counter = 0;
	Level *player;
};

class CrawlerCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new Crawler();
    }
};


#endif
