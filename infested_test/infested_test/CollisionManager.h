

#ifndef __SDL_Game_Programming_Book__CollisionManager__
#define __SDL_Game_Programming_Book__CollisionManager__

#include <iostream>
#include <vector>

class Player;
class GameObject;
class TileLayer;

class CollisionManager
{
public:
    
    void checkPlayerEnemyBulletCollision(Player* pPlayer);
    void checkPlayerEnemyCollision(Player* pPlayer, const std::vector<GameObject*> &objects);
    void checkEnemyPlayerBulletCollision(const std::vector<GameObject*>& objects);
    void checkPlayerTileCollision(Player* pPlayer, const std::vector<TileLayer*>& collisionLayers);
	void checkEnemyBulletTileCollision(Player* pPlayer, const std::vector<GameObject*> &objects, const std::vector<TileLayer*>& collisionLayers);
private:
	int playerX, playerY;
};

#endif /* defined(__SDL_Game_Programming_Book__CollisionManager__) */
