

#ifndef PLAYER_H
#define PLAYER_H

#include <iostream>
#include <vector>
#include "ShooterObject.h"
#include "GameObjectFactory.h"



class Player : public ShooterObject
{
public:
	
	static Player* Instance()
	{
		if (s_pInstance == 0)
		{
			s_pInstance = new Player();
			return s_pInstance;
		}
		return s_pInstance;
	}
    Player();
    virtual ~Player() {}
    
    virtual void load(std::unique_ptr<LoaderParams> const &pParams);
    
    virtual void draw();
    virtual void update();
    virtual void clean();
    
    virtual void collision();

	void usePower();

	int getX();
	int getY();

	void setPosition(int x,int y);
    
    virtual std::string type() { return "Player"; }

	//friend Zombie;
    
private:
    
    // bring the player back if there are lives left
    void ressurect();
    
    // handle any input from the keyboard, mouse, or joystick
    void handleInput();
    
    // handle any animation for the player
    void handleAnimation();

    // player can be invulnerable for a time
    int m_invulnerable;
    int m_invulnerableTime;
    int m_invulnerableCounter;

	//Bullet size
	int bulletSize = 11;

	//power up
	int powerupTime;
	int powerTimeCounter;
	bool powerupActive = false;
	int bulletpowerupcounter;

	static Player* s_pInstance;

	
};

// for the factory
class PlayerCreator : public BaseCreator
{
    GameObject* createGameObject() const
    {
        return new Player();
    }
};

typedef Player ThePlayer;


#endif /* defined(__SDL_Game_Programming_Book__Player__) */
